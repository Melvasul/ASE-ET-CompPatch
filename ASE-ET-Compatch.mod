version="1.3"
tags={
	"Expansion"
}
dependencies={
	"Ages and Splendor Expanded"
	"Extended Timeline"
}
name="CompPatch for ASE and ET"
supported_version="v1.37.*"
path="mod/ASE-ET-CompPatch"
remote_file_id="2593020513"